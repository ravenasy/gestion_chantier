# Titre du projet
_Gestion du Chantier_



Test technique symfony

## Pour commencer

git clone https://gitlab.com/ravenasy/gestion_chantier.git

### Pré-requis

- PHP =>7.1.3
- composer
- Mysql

### Installation

Les étapes pour installer ....

Modifier le fichier .env ligne 28 (ajout les informations de votre serveur Mysql ) .

_exemple : DATABASE_URL="mysql://root:@127.0.0.1:3306/gestion_chantier?serverVersion=5.7"

Executez la commande ``composer install`` pour installer les composant 

Executez la commande ``php bin/console d:d:c`` pour créer la base de donne

Executez la commande ``php bin/console d:s:u --force`` pour generer les tables

## Démarrage

-lancer la commande ``php -S localhost:8888 -t public``

-ouvre le lien localhost:8888

## Fabriqué avec


* [Bootstrap.css](https://getbootstrap.com/) - Framework CSS (front-end)
* [Symfony 4](https://symfony.com/) - Framework PHP (back-end)
* [VisualCode](https://code.visualstudio.com/) - Editeur de textes


## Auteurs

* **Brillant Ravenasy** _alias_ [@ravenasy](https://gitlab.com/ravenasy)
*mail bravenas@gmail.com


## License

Ce projet est sous licence ``exemple: WTFTPL`` - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations

