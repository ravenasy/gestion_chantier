<?php

namespace App\Controller;

use App\Entity\Pointage;
use App\Form\PointageType;
use App\Repository\PointageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Datetime;

/**
 * @Route("/pointage")
 */
class PointageController extends AbstractController
{

    private $pointageRepository;
    private $messageError;

    public function __construct(PointageRepository $pointageRepository){
        $this->pointageRepository = $pointageRepository;
        $this->messageError = '';
    }

    /**
     * @Route("/", name="pointage_index", methods={"GET"})
     */
    public function index(PointageRepository $pointageRepository): Response
    {
        return $this->render('pointage/index.html.twig', [
            'pointages' => $pointageRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pointage_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pointage = new Pointage();
        $form = $this->createForm(PointageType::class, $pointage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($this->validateForm($pointage)['state']){
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($pointage);
                $entityManager->flush();
                return $this->redirectToRoute('pointage_index');
            }else{
                $this->messageError = $this->validateForm($pointage)['message'];
            }
      
        }

        return $this->render('pointage/new.html.twig', [
            'pointage' => $pointage,
            'form' => $form->createView(),
            'error'=> $this->messageError
        ]);
    }

    /**
     * @Route("/{id}", name="pointage_show", methods={"GET"})
     */
    public function show(Pointage $pointage): Response
    {
        return $this->render('pointage/show.html.twig', [
            'pointage' => $pointage,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pointage_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pointage $pointage): Response
    {
        $form = $this->createForm(PointageType::class, $pointage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($this->validateForm($pointage , 'modification')['state']){
                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('pointage_index');
            }else{
                $this->messageError = $this->validateForm($pointage)['message'];
            }   
        }

        return $this->render('pointage/edit.html.twig', [
            'pointage' => $pointage,
            'form' => $form->createView(),
            'error'=> $this->messageError
        ]);
    }

    /**
     * @Route("/{id}", name="pointage_delete", methods={"POST"})
     */
    public function delete(Request $request, Pointage $pointage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pointage->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pointage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pointage_index');
    }

    /**
     * fontcion qui verifie la formualire
     */

    private function validateForm($pointage , $type = "insert") {
        $valeur = array('state' => true , "message" => '') ;
        
        $ckeckUserDay = $this->pointageRepository->findBy(
            [
                'utilisateur' => $pointage->getUtilisateur(),
                'date' => $pointage->getDate(),
                'chantier' => $pointage->getChantier()
            
            ]
        );
        if($ckeckUserDay && $type == 'insert') { 
           $valeur['message'] = "Un utilisateur ne peut pas être pointé deux fois le même jour sur le même chantier" ;
           $valeur['state'] = false;
        }else{
            $durationOfWeek = $this->pointageRepository->getDurationWeek($pointage->getUtilisateur(), $pointage->getDate()->format('Y-m-d H:i:s'));
            $somme = intval($durationOfWeek['duration']) + intval($pointage->getDuree());
            if($somme > 35 ) {
        
                $valeur['message'] = "La somme des durées des pointages d’un utilisateur pour une semaine ne pourra pas dépasser 35 
                heures" ;
                $valeur['state'] = false;
            }
        }
        return $valeur;
    }
}
