<?php

namespace App\Repository;

use App\Entity\Pointage;
use App\Entity\Utilisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pointage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pointage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pointage[]    findAll()
 * @method Pointage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PointageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pointage::class);
    }

    
    public function getDurationWeek(Utilisateur $user , String $dateString )
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.utilisateur = :user')
            ->andWhere('WEEK(p.date) = WEEK(:date)')
            ->setParameter('user', $user)
            ->setParameter('date', $dateString)
            ->select('SUM(p.duree) as duration')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
}
